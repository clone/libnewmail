/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

#include "util.h"
#include "newmail.h"

char* nm_chomp(char *ln) {
    if (!ln)
        return NULL;
    ln[strcspn(ln, "\n\r\0")] = 0;
    return ln;
}

char* nm_specials(const char *format) {
    int i;
    static char ret[PATH_MAX];
    const char *p;
    char *d;
    int special=0, max;

    if (!format)
        return NULL;
    
    max = PATH_MAX-1;
    ret[max] = 0;

    memset(ret, 0, max);
    
    for (p = format, d = ret, i=0; *p && i < max; p++) {
        //fprintf(stderr, "CHAR: %c [%s] %s\n", *p, ret, special ? "SPEC" : "");
        if (special) {
            char *a = NULL;
            if (*p == 'u')
                a = getenv("USER");
            else if (*p == 'h')
                a = getenv("HOME");
            else if (*p == 'H') {
                static char hn[256];
                gethostname(a = hn, sizeof(hn));
            }
                            
            if (a) {
                int x;
                special = 0;
                strncpy(d, a, max-i);
                x = strlen(d);
                d += x; i += x;
                continue;
            }
        } else if (*p == '%') {
            special = 1;
            continue;
        }

        special = 0;
        *d = *p;
        d++; i++;
    }

    return ret;
}


char *nm_strdup(const char *s) {
    char *p;
    if (!s)
        return 0;
    p = nm_malloc(strlen(s)+1);
    return p ? strcpy(p, s) : 0;
}

void nm_error(enum nm_error en, const char* exp) {
    nm_errno = en | (exp ? NM_ERROR_EXPLANATION : 0);
    if (nm_explanation)
        nm_free((void*) nm_explanation);
    nm_explanation = nm_strdup(exp);
}


