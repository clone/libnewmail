/* $Id: nmail.c 23 2003-06-04 17:16:43Z lennart $ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <stdio.h>
#include <errno.h>

#include <newmail.h>

int main(int argc, char *argv[]) {
    struct nm_spool* s = NULL;
    struct nm_status st;
    int r = 1;

    /* Open mail spool */
    if (!(s = nm_open(argc > 1 ? argv[1] : NULL))) {
        fprintf(stderr, "nm_open(): %s\n", nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Query for status */
    if (nm_query(s, NM_QUERY_CUR|NM_QUERY_NEW, &st) < 0) {
        fprintf(stderr, "nm_check(): %s\n", nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Print results */
    if (st.new > 0)
        printf("You have new mail\n");
    else if (st.cur > 0)
        printf("You have mail\n");
    else
        printf("No mail\n");
    
    r = 0;
    
finish:

    /* Close mail spool */
    if (s)
        nm_close(s);
        
    return r;
}
