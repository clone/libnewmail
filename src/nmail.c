/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <stdio.h>
#include <errno.h>

#include <newmail.h>

/* This type of query is issued */
int query = NM_QUERY_NCUR|NM_QUERY_NNEW;

/* A simple function formatting mail status codes */
char *b2s(int b) {
    if (b < 0) return "fail";
    if (b > 0) return "yes";
    return "no";
}

/* A callback function called once for each defined libnewmail mailbox */
void cb(const char *spool, void*user) {
    struct nm_spool* s = NULL;
    struct nm_info i;
    struct nm_status st;

    /* Open this mail spool */
    if (!(s = nm_open(spool))) {
        fprintf(stderr, "nm_open(\"%s\"): %s\n", spool, nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Get spool information */
    if (nm_info(s, &i) < 0) {
        fprintf(stderr, "nm_info(\"%s\"): %s\n", spool, nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Show some information about the spool */
    printf("\n%s:\n\tName: %s\n\tType: %s\n\tText: %s\n\tFlags: ", i.path, i.name, i.type, i.text);
    if (i.flags & NM_FLAG_SYNCHRONOUS) printf("SYNCHRONOUS ");
    if (i.flags & NM_FLAG_ASYNCHRONOUS) printf("ASYNCHRONOUS ");
    printf("\n");

    /* Do a status query */
    if (nm_query(s, query, &st) < 0) {
        fprintf(stderr, "nm_check(\"%s\"): %s\n", spool, nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Print the status query results: total number of mails */
    if (query & NM_QUERY_NCUR)
        printf("\tCurrent: %i\n", st.cur);
    else if (query & NM_QUERY_CUR)
        printf("\tCurrent: %s\n", b2s(st.cur));

    /* Print the status query results: new mails */
    if (query & NM_QUERY_NNEW)
        printf("\tNew: %i\n", st.new);
    else if (query & NM_QUERY_NEW)
        printf("\tNew: %s\n", b2s(st.new));

finish:

    /* Close the mail spool */
    if (s)
        nm_close(s);
}

int main(int argc, char *argv[]) {

    /* Iterate through the mail spools */
    if (nm_list(cb, NULL) < 0)
        nm_perror("nm_list()");

    return 0;
}
