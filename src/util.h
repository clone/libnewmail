#ifndef fooutilhfoo
#define fooutilhfoo

/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

/** \file
 *
 * Some utility functions which may be used by plugins
 */

#include "newmail.h"

/** Remove all newline and carriage return characters from the end of
 * the string
 * @param ln The string to chomp
 * @return The same string, chomped
 */
char* nm_chomp(char *ln);

/** Replace all special characters like "%%h" by their respective
 * meanings.
 * @param format The format string to expand
 * @return A pointer to a static memory region containing the expanded string
 */
char* nm_specials(const char *format);

/** Like libc's strdup() but uses nm_malloc() for memory allocation
 * @param s The string to duplicate
 * @return A pointer to a newly allocated memory block containing the copied string */
char *nm_strdup(const char *s);

/** Set the current error condition to the given values 
 * @param en Specifies the error condition to set
 * 
 * @param exp Specifies an explanation for the error, if sensible. May
 * be NULL. NM_ERROR_EXPLANATION is set iff exp is not NULL.
 */
void nm_error(enum nm_error en, const char* exp);

#endif
