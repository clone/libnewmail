#ifndef foonewmailhfoo
#define foonewmailhfoo

/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

/** \mainpage
 *
 * For a brief explanation of libnewmail's purpose, have a look on <a href="../../README.html">the README file</a>. Thank you!
 * 
 */

/** \example easy.c
 * A minimal example
 */

/** \example nmail.c
 * This is an usage example for the simple, synchronous API of libnewmail
 */

/** \example nmail-async.c
 * This is an usage example for the asynchronous API of libnewmail
 */

/** \example nm-spoolhack.c This is an example implementing a tool to
 * emulate Unix mail spool stat() behaviour for a flag file to
 * multiplex mail status information of libnewmail plugins.
 */


/** \file
 *
 * The public application interface of libnewmail. Applications
 * linking to libnewmail should use only functions defined in this
 * header file.
 */

#include <limits.h>
#include <oop.h>

/**
 *  Flags for a mail spool; specifies if a query on this spool will be
 *  executed synchronously or asynchronously. Local mailbox queries
 *  are probably executed synchronously, while networked queries are
 *  exectued asynchronously.
 */
enum nm_flags {
    NM_FLAG_SYNCHRONOUS= 1,            /**< The spool query won't block */
    NM_FLAG_ASYNCHRONOUS = 2           /**< The spool query may block when not issued asynchronously */
};

/** 
 * An enumeration specifying a certain error
 * condition. NM_ERROR_SYSTEM and NM_ERROR_EXPLANATION may be ORed
 * logically with one of the other constants, to specifiy that system
 * errno and/or the nm_explanation variable is in context with the
 * error occured.
 */
enum nm_error {
    NM_ERROR_SYSTEM = 256,                 /**< When this bit is set, errno is also set */
    NM_ERROR_EXPLANATION = 512,            /**< When this bit is set, nm_explanation is set */
    NM_ERROR_SUCCESS = 0,                  /**< The query succeeded */
    NM_ERROR_NOCONFIG = 1,                 /**< No configuration file was found for this spool */
    NM_ERROR_INVPAR = 2,                   /**< An API function was called with corrupt parameters */
    NM_ERROR_MEMORY = 3,                   /**< Failure while allocating memory */ 
    NM_ERROR_INVNAME = 4,                  /**< Invalid name */
    NM_ERROR_DLFAIL = 5,                   /**< Plugin could not be loaded (failure of dynamic loader) */
    NM_ERROR_NOTIMPL = 6,                  /**< Function not implemented */
    NM_ERROR_NOFILE = 7,                   /**< File not found */
    NM_ERROR_FORK = 8,                     /**< Failure on fork() */
    NM_ERROR_ALREADY = 9,                  /**< A query was submitted while the previous hasn't been terminated yet */
    NM_ERROR_CONTEXT = 10,                 /**< Function called in wrong context */
    NM_ERROR_INTERNAL = 11,                /**< Internal error */
    NM_ERROR_SERVFAIL = 12,                /**< Server failed */ 
    NM_ERROR_SERVNOTFOUND = 13,            /**< Server not found */
};

/**
 * Types of mail spool queries; A combination of these flags should be
 * specified on a call to nm_query()
 */
enum nm_query {
    NM_QUERY_CUR = 1,                      /**< Query the boolean availability of total (current) mails */  
    NM_QUERY_NEW = 2,                      /**< Query the boolean availabillty of unread mails */
    NM_QUERY_NCUR = 4,                     /**< Query the numeric count of total (current) mails */
    NM_QUERY_NNEW = 8                      /**< Query the numeric count of unread mails */
};

/** Opaque structure encapsulating a handle on a mail spool A pointer
 * to a structure of this type is returned by nm_open(). It should be
 * freed with nm_close().
 */
struct nm_spool;

/** A structure describing a mail spool status.
 *
 */
struct nm_status { 
    int cur;                               /**< The number of total mails available if the query was issued with NM_QUERY_NCUR or a boolean value if NM_QUERY_CUR was set. */  
    int new;                               /**< The number of unread mails available if the query was issued with NM_QUERY_NNEW or a boolean value if NM_QUERY_NEW was set. */  
};

/** A structure for storing information about a mail spool. For usage with nm_info().
 */
struct nm_info {
    char name[NAME_MAX];                   /**< Name of the mailspool */
    char path[PATH_MAX];                   /**< Path to the configuration file */
    char type[32];                         /**< Textual representation of the mail spool type */
    char text[128];                        /**< Description of the mail spool */
    enum nm_flags flags;                   /**< Flags describing the the mail spool */
};


/** A prototype for callback functions for enumerating configured
 * mail spools with nm_list().
 * @param spool A path to a configuration file
 * @param user The user pointer specified on nm_list() invocation
 */
typedef void (*nm_enum_cb_t) (const char *spool, void*user);

/** A prototype for callback functions for asynchronous mail spool queries.
 * @param s The mail spool belonging to this response
 * @param status A pointer to a structure describing the mail spool status or NULL, if an error occured. In this case, nm_errno is set.
 * @param user The user pointer specified on nm_query_submit() invocation
 */
typedef void (*nm_query_cb_t) (struct nm_spool *s, struct nm_status *status, void *user);

/** A pointer to a malloc() compatible function which is used by
 * libnewmail for allocating memory. Initially set to libc's malloc().
 */
extern void* (*nm_malloc)(size_t);

/** A pointer to a realloc() compatible function which is used by
 * libnewmail for reallocating memory. Initially set to libc's
 * realloc().
 */
extern void* (*nm_realloc)(void *,size_t); 

/** A pointer to a free() compatible function which is used by
 * libnewmail for freeing memory. Initially set to libc's
 * free().
 */
extern void (*nm_free)(void *);

/** A pointer to a textual string giving a terse explanation for the
 * last failure. This is only valid when NM_ERROR_EXPLANATION is set
 * in nm_errno.
 */
extern const char *nm_explanation;

/** A variable describing the last error occured. Only valid when the
 * last API function call returned a failure.
 */
extern enum nm_error nm_errno;


/** Open the given mail spool configuration file and return a pointer
 * to an opaque stracture used as handle to access the mail spool. The
 * returned pointer should be freed with nm_close() when it is no
 * longer needed.
 * @param spool A path to a mail spool configuration file. If NULL an
 * automatic detection of the mail spool location is tried.
 * @return A pointer to a newly allocated spool handle or NULL on failure. In this case nm_errno is set.
 */
struct nm_spool* nm_open(const char *spool);

/** Free the given mail spool handle.
 * @param s The spool handle to be freed
 */
void nm_close(struct nm_spool *s);

/** Issue a synchronous query for the specified mail spool. The
 * function will block until the query succeeded or a failure is
 * returned. Be aware that every query may use fork() to spawn a
 * background process to execute the query.
 * @param s The spool to be queried
 * @param query The desired query type
 * @param status A pointer to a structure which will be filled with the query response
 * @return zero on success, negative on failure. In the latter case nm_errno is set.
 */
int nm_query(struct nm_spool *s, enum nm_query query, struct nm_status *status);

/** Issue an asynchronous query for the specified mail spool. The
 * function will return immediately, however the callback cb is called
 * when the query is finished. Be aware that every query may use fork() to spawn a
 * background process to execute the query.
 * @param s The spool to be queried
 * @param query The desired query type
 * @param oop The liboop oop_source which shall be used for asynchronous handling
 * @param cb The callback function to be called when the query is finished.
 * @param user An arbitrary pointer to be passed to cb
 * @return zero on success, negative on failure. In the latter case nm_errno is set.
 */
int nm_query_submit(struct nm_spool *s, enum nm_query query, oop_source *oop, nm_query_cb_t cb, void *user);

/** Show an X11 configuration dialog for the specified mail spool -- Currently an NOOP
 * This function will run an external configuration program for the specific mail spool eventually.
 * @param s The spool the dialog should be shown for. NULL if a
 * complete configuration dialog should be shown, with the possibility
 * for the user to create new mail spool profiles.
 * @return zero on success, negative on failure. In the latter case nm_errno is set.
 */
int nm_configure(struct nm_spool *s);

/** Fill a structure containing some information about the specified mail spool
 * @param s The spool to be queried
 * @param info A pointer to the information structure to be filled
 * @return zero on success, negative on failure. In the latter case nm_errno is set.
 */
int nm_info(struct nm_spool *s, struct nm_info *info);

/** Enumerate all configured mail spools.
 * @param cb A callback function called for every configured mail spool.
 * @param user An arbitrary pointer to be passed to cb
 * @return negative on failure, number of calls to cb invoked. If this
 * is zero, no mail spool is configured, you probably should try a
 * nm_open(NULL) next.
 */
int nm_list(nm_enum_cb_t cb, void *user);

/** Return a textual representation of the given error triplet.
 * @param n A libnewmail error condition (e.g. nm_errno)
 * @param e A libc error condition (e.g. errno)
 * @param explanation A libnewmail error explanation (e.g. nm_explanation)
 * @return A pointer to a string in static memory. The contents is
 * overwritten on subsequent calls to nm_strerror() or nm_perror().
 */
const char *nm_strerror(enum nm_error n, int e, const char *explanation);

/** A similar function to libc's perror(). This function will show
 * libnewmail error conditions however.
 * @param s A string which will be concatenated with the error string
 */
void nm_perror(const char *s);

#endif
