/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "newmail.h"
#include "util.h"

config_t* nm_config_open(const char *fn) {
    config_t* c = NULL;
    FILE* f = NULL;

    if (!(c = nm_malloc(sizeof(config_t)))) {
        nm_error(NM_ERROR_MEMORY, NULL);
        goto fail;
    }

    if (!fn) {
        c->file = NULL;
        return c;
    }
    
    if (!(f = fopen(fn, "r"))) {
        nm_error(NM_ERROR_NOFILE|NM_ERROR_SYSTEM, "Configuration file not found\n");
        goto fail;
    }

    c->file = f;
    return c;

fail:
    if (c)
        nm_free(c);

    if (f)
        fclose(f);

    return NULL;
}

void nm_config_close(config_t *c) {
    if (c) {
        if (c->file)
            fclose(c->file);

        nm_free(c);
    }
}


const char *nm_config_get(config_t *c, const char*entry, const char*def) {
    if (!c || !entry || !c->file)
        return def;

    rewind(c->file);

    while (!feof(c->file)) {
        static char ln[128];
        char *n, *t;

        if (!fgets(ln, sizeof(ln), c->file))
            return def;

        nm_chomp(ln);

        n = &ln[strspn(ln, " \t")];

        if (*n == '#' || *n == 0)
            continue;

        if (!(t = strpbrk(n, " \t")))
            t = strchr(n, 0);

        if (strncmp(n, entry, t-n) == 0) {
            t += strspn(t, " \t");

            return t;
        }
    }

    return def;
}


int nm_config_get_int(config_t *c, const char*entry, int def) {
    const char *r;

    if ((r = nm_config_get(c, entry, NULL)))
        return atoi(r);

    return def;
}


int nm_config_get_bool(config_t *c, const char*entry, int def) {
    const char *r;

    if ((r = nm_config_get(c, entry, NULL)))
        return tolower(*r) == 'y' || atoi(r) || *r == '+' || !strcasecmp(r, "on");

    return def;
}
