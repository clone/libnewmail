#ifndef foomodulehfoo
#define foomodulehfoo

/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

/** \file
 *
 * The plugin interface of libnewmail. Mail checking modules should
 * use these functions and those declared in config.h and util.h
 */

#include <ltdl.h>
#include "newmail.h"
#include "config.h"

/** A structure encapsulating information about a mail spool handle. It is opaque to the application 
 */
struct nm_spool {
    lt_dlhandle dl;                    /**< A libltdl handle to the plugin so */
    char *data;                        /**< Some plugin specific data */
    config_t *config;                  /**< A configuration context for the mail spool */
    char *path;                        /**< The path to the associated configuration file */

    int (*query) (struct nm_spool *s, enum nm_query query, struct nm_status *status);
    /**< A pointer to the real query() function wrapped by nm_query() */
    
    int (*query_submit) (struct nm_spool *s, enum nm_query query, oop_source* oop, nm_query_cb_t cb, void *user);
    /**< A pointer to the real query_submit() function wrapped by nm_query_submit() */
    
    int (*configure) (struct nm_spool *s);
    /**< A pointer to the real configure() function wrapped by nm_configuret() */
    
    int (*info) (struct nm_spool *s, struct nm_info *i);
    /**< A pointer to the real query_info() function wrapped by nm_info() */
    
    void (*done) (struct nm_spool *s);
    /**< A pointer to a function which will be called before this nm_spool is freed. */
};

/** The prototype of the only function which needs to be exported by a
 * plugin. It is called when new nm_spool of the plugin is
 * instantiated.
 * @param s The virgin nm_spool structure. The plugin should fill in
 * the pointers to functions contained in the structure.\c dl, \c config and \c path are
 * already filled in, \c data may be used for arbitrary spool handle
 * specific data,
 * @return zero on success, nonzero on failure.
 */
typedef int (*nm_init_t) (struct nm_spool *s);

#endif
