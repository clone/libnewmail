/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include <newmail.h>

/* The query to issue */
int query = NM_QUERY_NCUR|NM_QUERY_NNEW;

/* A counter for the queries running */
int count = 0;

/* The liboop main loop object */
oop_source *oop = NULL;

/* A linked list structure for the spools */
struct spool_ll{
    struct nm_spool *spool;
    struct spool_ll *next;
} *spools = NULL;

/* A function which converts binary mail spool status flags into human readable strings */
char *b2s(int b) {
    if (b < 0) return "fail";
    if (b > 0) return "yes";
    return "no";
}

/* A callback function for oop which terminates the main loop */
void* _finish(oop_source *source, struct timeval tv, void *user) {
    return OOP_HALT;
}

/* A utility function which issues the callback defined above */
void finish(void) {
    oop->on_time(oop, OOP_TIME_NOW, _finish, NULL);
}

/* A callback function which is called whenever a mail spool query finished */
void cb_check(struct nm_spool *s, struct nm_status *status, void *user) {
    struct nm_info i;
    static char txt[256];

    /* Check the status flag */
    if (!status)
        snprintf(txt, sizeof(txt), "%s", nm_strerror(nm_errno, errno, nm_explanation));

    /* Get some information about the mail spool */
    if (nm_info(s, &i) < 0) {
        fprintf(stderr, "nm_open(): %s\n", nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Print that information */
    printf("\n%s:\n\tName: %s\n\tType: %s\n\tText: %s\n\tFlags: ", i.path, i.name, i.type, i.text);
    if (i.flags & NM_FLAG_SYNCHRONOUS) printf("SYNCHRONOUS ");
    if (i.flags & NM_FLAG_ASYNCHRONOUS) printf("ASYNCHRONOUS ");
    printf("\n");

    /* Show status information */
    if (status) {
        if (query & NM_QUERY_NCUR)
            printf("\tCurrent: %i\n", status->cur);
        else if (query & NM_QUERY_CUR)
            printf("\tCurrent: %s\n", b2s(status->cur));

        if (query & NM_QUERY_NNEW)
            printf("\tNew: %i\n", status->new);
        else if (query & NM_QUERY_NEW)
            printf("\tNew: %s\n", b2s(status->new));
        
    } else
        fprintf(stderr, "\n\tFAILURE: %s\n", txt);
    
finish:

    count--;
    if (count <= 0)
        finish();
}

/* The callback function for iterating through the mail spools available */
void cb_list(const char *spool, void *user) {
    struct nm_spool* s = NULL;
    struct spool_ll* l = 0;

    /* Open the mail spool */
    if (!(s = nm_open(spool))) {
        fprintf(stderr, "nm_open(\"%s\"): %s\n", spool, nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Allocate a linked list entry */
    if (!(l = malloc(sizeof(struct spool_ll)))) {
        fprintf(stderr, "Memory\n");
        goto finish;
    }

    /* Start the spool query */
    if (nm_query_submit(s, query, oop, cb_check, NULL) < 0) {
        fprintf(stderr, "nm_check_submit(\"%s\"): %s\n", spool, nm_strerror(nm_errno, errno, nm_explanation));
        goto finish;
    }

    /* Fill in the linked list entry */
    l->spool = s;
    l->next = spools;
    spools = l;

    count++;
    
    return;
    
finish:

    free(l);

    if (s)
        nm_close(s);
}

int main(int argc, char *argv[]) {
    oop_source_sys *sys;
    count = 1; /* This variable is initialized to 1 because otherwise
                * the main loop is terminated too early when
                * synchronous mail spools are queried */

    /* Create a generic liboop main loop object */
    if (!(sys = oop_sys_new())) {
        fprintf(stderr, "Could not reate OOP system source.\n");
        return 1;
    }

    if (!(oop = oop_sys_source(sys))) {
        fprintf(stderr, "Could not reate OOP system source.\n");
        oop_sys_delete(sys);
        return 1;
    }

    /* Iterate through the mail spools avilable */
    if (nm_list(cb_list, NULL) < 0)
        nm_perror("nm_list()");

    count--;

    /* Wait until all queries finished */
    while (count > 0)
        oop_sys_run(sys);

    /* Shutdown the main loop */
    oop_sys_delete(sys);

    /* Free the linked list */
    while (spools) {
        struct spool_ll *l = spools;
        spools = spools->next;
        nm_close(l->spool);
        free(l);
    }

    return 0;
}
