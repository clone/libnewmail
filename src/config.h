#ifndef fooconfighfoo
#define fooconfighfoo

/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

/** \file
 *
 * Some routines to ease the configuration file access. This is only
 * used by the plugins, not by applications linking to libnewmail.
 */


#include <stdio.h>

/** A configuration file handle
 */
typedef struct {
    FILE *file;  /**< A FILE pointer to the configuraiton file */
} config_t;

/** Open the specified configuration file
 * @param fn Path to configuration file
 * @return A handle to the configuration file object
 */
config_t* nm_config_open(const char *fn);

/** Close a configuration file oject
 * @param c The configuration file object to be freed
 */
void nm_config_close(config_t *c);

/** Request a specific configuration key from the configuration file.
 * @param c The configuration file object
 * @param entry The configuration file key
 * @param def The default value, when the key is not found. May be NULL
 * @return A pointer to a statically allocated string containing the value read or def.
 */
const char *nm_config_get(config_t *c, const char*entry, const char* def);

/** Same as nm_config_get() but requests an integer
 * @param c The configuration file object
 * @param entry The configuration file key
 * @param def The default value, when the key is not found. 
 * @return The value read or def
 */
int nm_config_get_int(config_t *c, const char*entry, int def);

/** Same as nm_config_get() but requests a boolean
 * @param c The configuration file object
 * @param entry The configuration file key
 * @param def The default value, when the key is not found. 
 * @return The value read or def
 */
int nm_config_get_bool(config_t *c, const char*entry, int def);

#endif
