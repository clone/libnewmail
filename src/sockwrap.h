#ifndef foosockwraphfoo
#define foosockwraphfoo

/* $Id$ */

/***
  This file is part of libnewmail

  libnewmail is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  libnewmail is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with libnewmail; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
  USA
***/

struct sockwrap;

struct sockwrap* sockwrap(char *host, int port, int tls);
void sockwrap_close(struct sockwrap *s);
int sockwrap_writeln(struct sockwrap *s, char *ln);
int sockwrap_readln(struct sockwrap *s, char *ln, int max);

#endif
