#!/bin/sh
# $Id: bootstrap.sh 16 2003-07-06 12:15:37Z lennart $

# This file is part of libnewmail.
#
# libnewmail is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# libnewmail is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libnewmail; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

if [ "x$1" = "xam" ] ; then
    set -ex
    automake -a -c --foreign
    ./config.status
else 
    set -ex

    rm -rf autom4te.cache
    rm -f config.cache

    aclocal
    libtoolize -c --force
    autoheader
    automake -a -c
    autoconf -Wall

    ./configure --sysconfdir=/etc "$@"

    make clean
fi

